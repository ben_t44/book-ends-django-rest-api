import React from 'react';
import { Link } from "react-router-dom";
import BookTile from '../BookTile/BookTile';


class BookView extends React.Component {

    constructor(props) {
        super(props);

    }


    render() {
        return(
            <div class='container'>
                <div class='row'>
                    {this.props.data.map((book,index) => {
                        return (<BookTile isbn={book.isbn} />);
                    })}
                </div>
            </div>
        );
    }
}

export default BookView;