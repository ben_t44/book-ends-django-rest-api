import React from 'react';


class BookTile extends React.Component {

    constructor(props) {
        super(props);
        // Don't call this.setState() here!
        this.state = { book: null };
      }
    
    componentDidMount() {
        var isbn = this.props.isbn || "9781451648546"; // Steve Jobs book 
    
        var url = "https://www.googleapis.com/books/v1/volumes?q=isbn:" + isbn;
        
        fetch(url)
            .then(res=>res.json())
            .then(
                (results) => {
                    console.log(results);
                    if (results.totalItems) {
                        
                        // There'll be only 1 book per ISBN
                        var book = results.items[0];
                        
                        var title = (book["volumeInfo"]["title"]);
                        var subtitle = (book["volumeInfo"]["subtitle"]);
                        var authors = (book["volumeInfo"]["authors"]);
                        var printType = (book["volumeInfo"]["printType"]);
                        var pageCount = (book["volumeInfo"]["pageCount"]);
                        var publisher = (book["volumeInfo"]["publisher"]);
                        var description = (book["volumeInfo"]["description"]);
                        var image = (book["volumeInfo"]["imageLinks"]['thumbnail']);
                    
                        this.setState({book:  {
                                                    'title': title,
                                                    'subtitle': subtitle,
                                                    'authors': authors,
                                                    'printType': printType,
                                                    'pageCount': pageCount,
                                                    'publisher': publisher,
                                                    'description': description,
                                                    'image': image
                                                }
                                            });
                    }
                
                });

    }

    render() {
        if (this.state.book) {
            return(
                <div className="card bg-light mb-3 col-4">
                    <div className="card-body">
                        <h4 className="card-title">{this.state.book.title}</h4>
                        <h6><small class="text-muted">{"by: "}
                          {this.state.book.authors.map((author,index) => {
                                    return author+ ', ';
                                })}</small></h6>
                        <img src={this.state.book.image} alt='Cover Art'></img>
                        <p className="card-text">{this.state.book.description}</p>
                    </div>
                </div>
    
            );
        }
        return(
            <div className="">
            </div>

        );
    }
}

export default BookTile;
