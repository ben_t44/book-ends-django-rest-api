import React from 'react';
import { Link } from "react-router-dom";

const formStyle = {
    margin: '30px'
  };

var axios = require("axios");
axios.defaults.withCredentials = true;


class BookForm extends React.Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {message:null}
    }

    handleChange(event) {
        this.setState({inputfield : event.target.value});
      }

    handleSubmit(event) {
        event.preventDefault();
        
        if(!this.state.inputfield){
            return;
        }
        var isbn = this.state.inputfield
    
        var url = "https://www.googleapis.com/books/v1/volumes?q=isbn:" + isbn;
        
        let currentComponent = this;
        fetch(url)
            .then(res=>res.json())
            .then(
                (results) => {
                    console.log(results);
                    if (results.totalItems) {
                        
                        // There'll be only 1 book per ISBN
                        var book = results.items[0];
                        
                        var title = (book["volumeInfo"]["title"]);
                        var authors = (book["volumeInfo"]["authors"]);
                        var postData =  {
                                            'title': title,
                                            'author': authors[0],
                                            'isbn': isbn,
                                            "book_status": "h",
                                            "book_condition": "h"
                                        }

                        axios.post('http://localhost:8000/api/Books/', { 
                            'title': title,
                            'author': authors[0],
                            'isbn': isbn,
                            "book_status": "h",
                            "book_condition": "h"
                        })
                            .then(function (response) {
                                console.log(response);
                                currentComponent.setState({message:"success"});
                            })
                            .catch(function (error) {
                                console.log(error);
                                currentComponent.setState({message:"failure"});
                            });
                        
                    } else {
                        this.setState({message:"failure"});
                    }
                
                });
    }

    render() {
        var displayMessage;

        if(this.state.message==='failure') {
            displayMessage = (
                <div class="alert alert-dismissible alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Oh snap!</strong>  That book doesn't exist. Try again.
                </div>
            )
        } else if(this.state.message==='success') {
            displayMessage = <div class="alert alert-dismissible alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Well done!</strong> You successfully read that book.
            </div>
        }

        return(
            <form ref="form">
                <fieldset>
                    {displayMessage}
                    <div className="text-center">
                        <h1>Book Entry Form</h1>
                        <p class="lead">Book insertion is very easy. Just enter the book's ISBN and the edition and copy information will automatically be added.</p>
                    </div>
                    <div class="form-group text-center" style={formStyle}>
                        <div className='form-group row'>
                            <label for="staticEmail" className="col-form-label">Enter the ISBN:</label>
                            <div class="col-sm-4">
                                <input type="text" className="form-control" onChange={this.handleChange} />
                            </div>
                            <div>
                                <button onClick={this.handleSubmit} className='btn btn-primary'>Enter</button>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        );
    }
}

export default BookForm;