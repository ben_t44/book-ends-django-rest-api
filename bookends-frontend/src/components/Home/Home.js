import React from 'react';
class Home extends React.Component {

    render() {

        return (
            <div>

                <section class="jumbotron text-center">
                    <div class="container">
                        <h1>Bookends International</h1>
                        <p class="lead text-muted">Bookends International is a non-profit organisation whose primary mission is sending books to TCKs at the Ends of the Earth</p>
                        <p>
                            <a href="/Books" class="btn btn-primary my-2">Request a Book</a>
                            <a href="/bookform" class="btn btn-secondary my-2">Enter a Book</a>
                        </p>
                    </div>
                </section>

            </div>
        );
    }
}

export default Home;