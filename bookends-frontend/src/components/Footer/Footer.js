import React from 'react';
class Footer extends React.Component {

    render() {

        return (
            <footer class="text-muted">
                <div class="container">
                    <p class="float-right">
                        <a href="/">Refresh Page</a>
                    </p>
                    <p>Westmont College CS-125 Pine &copy; </p>
                </div>
            </footer>
        );
    }
}

export default Footer;