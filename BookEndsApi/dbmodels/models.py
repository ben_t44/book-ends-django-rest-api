from django.db import models
# TODO:
#	set primary and unique values
#	change zip var in Persons
#	chance picture field in Requesters
#	make automatic dates where appropriate


# Create your models here.

class Persons(models.Model):
    person_id = models.IntegerField              # may need to do double if INT is to small
    lname = models.CharField(max_length=40)
    fname = models.CharField(max_length=40)
    phone = models.CharField(max_length=20)
    street = models.CharField(max_length=50) 
    street2 = models.CharField(max_length=50)
    street3 = models.CharField(max_length=50)
    zip = models.CharField(max_length=20)        #must change name of zip var
    city = models.CharField(max_length=20)
    state = models.CharField(max_length=50)
    email = models.CharField(max_length=60)
    country = models.CharField(max_length=60)
    countrycode = models.CharField(max_length=10)
    username = models.CharField(max_length=30)
    company_name = models.CharField(max_length=60) 
    password = models.CharField(max_length=60)
    
    def __str__(self):
        return self.person_id


class Donors(models.Model): 
    donor_id = models.IntegerField
    person_id = models.IntegerField
    needs_receipt = models.BooleanField

    def __str__(self):
        return self.donor_id


class Donations(models.Model):
    donation_id = models.IntegerField()
    dt_acknowledged = models.DateField        #to make auto DateField(auto_now_add=True) 
    dt_donated = models.DateField
    amt_donated = models.DecimalField(max_digits=7, decimal_places=2)
    donor_id = models.IntegerField

    def __str__(self):
        return self.donation_id

class Books(models.Model):
    title = models.CharField(max_length=60)
    author = models.CharField(max_length=60)
    book_id = models.IntegerField
    book_status = models.CharField(max_length=20)
    book_condition = models.CharField(max_length=20)
    isbn = models.CharField(max_length=30, default="0")
    lt_id = models.IntegerField
    donation_id = models.IntegerField
    request_id = models.IntegerField
    edition_id = models.IntegerField

    def __str__(self):
        return self.title

class Editions(models.Model):
    edition_id = models.IntegerField
    work_id = models.IntegerField
    amazon_work_id = models.IntegerField
    lt_work_id = models.IntegerField
    google_work_id = models.IntegerField
    dt_published = models.DateField
    publisher = models.CharField(max_length=30)
    isbn = models.CharField(max_length=30)
    cover = models.CharField(max_length=20)
    category = models.CharField(max_length=20)
    requester_id = models.IntegerField

    def __str__(self):
        return self.edition_id

class Staff(models.Model):
    staff_id = models.IntegerField
    role = models.CharField(max_length=40)
    person_id = models.IntegerField

    def __str__(self):
        return self.staff_id

class Requesters(models.Model):
    requester_id = models.IntegerField
    how_heard = models.CharField(max_length=255)
    lt_id = models.IntegerField
    picture = models.CharField(max_length=255)      #change to ImageField for img https://docs.djangoproject.com/en/2.2/ref/models/fields/#imagefield
    person_id = models.IntegerField

    def __str__(self):
        return self.requester_id

class Kids(models.Model):
    child_id = models.IntegerField
    name = models.CharField(max_length=60)
    birth_year = models.CharField(max_length=4)
    interests = models.CharField(max_length=255)
    reading_level = models.CharField(max_length=255)
    requester_id = models.IntegerField
    dt_updated = models.DateField

    def __str__(self):
        return self.child_id

class Requests(models.Model):
    request_id = models.IntegerField
    dt_requested = models.DateField
    boxes = models.CharField(max_length=30)
    requirements = models.CharField(max_length=140)
    delivery_method = models.CharField(max_length=40) 
    pull_sheet = models.CharField(max_length=140)
    order_status = models.CharField(max_length=20)
    ship_details = models.CharField(max_length=140)
    amt_shipping = models.DecimalField(max_digits=7, decimal_places=2)
    amt_owed = models.DecimalField(max_digits=7, decimal_places=2)
    amt_paid = models.DecimalField(max_digits=7, decimal_places=2)
    dt_survey = models.DateField 
    dt_pulled = models.DateField
    dt_shipped = models.DateField
    dt_received = models.DateField
    dt_paid = models.DateField
    courier_id = models.IntegerField
    book_id = models.IntegerField
    requester_id = models.IntegerField
    ship_address = models.CharField(max_length=240)

    def __str__(self):
        return self.request_id