import React from 'react';
import { Link } from "react-router-dom";


class About extends React.Component {

    render() {
        return(
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href='/'>Navbar</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarColor03">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <Link to="/about"><div className='nav-link'>About</div></Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/books"><div className='nav-link'>Books</div></Link>
                        </li>
                    </ul>
                </div>
            </nav>

        );
    }
}

export default About;