

from dbmodels import models 
from .serializers import BooksSerializer, DonationsSerializer, DonorsSerializer, EditionsSerializer, KidsSerializer, PersonsSerializer, RequestersSerializer, RequestsSerializer, StaffSerializer
from rest_framework import generics
#from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend

class BooksListCreate(generics.ListCreateAPIView):
    queryset = models.Books.objects.all()
    serializer_class = BooksSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = '__all__'

class DonationsListCreate(generics.ListCreateAPIView):
    queryset = models.Donations.objects.all()
    serializer_class = DonationsSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = '__all__'

class DonorsListCreate(generics.ListCreateAPIView):
    queryset = models.Donors.objects.all()
    serializer_class = DonorsSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = '__all__'

class EditionsListCreate(generics.ListCreateAPIView):
    queryset = models.Editions.objects.all()
    serializer_class = EditionsSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = '__all__'

class KidsListCreate(generics.ListCreateAPIView):
    queryset = models.Kids.objects.all()
    serializer_class = KidsSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = '__all__'

class PersonsListCreate(generics.ListCreateAPIView):
    queryset = models.Persons.objects.all()
    serializer_class = PersonsSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = '__all__'

class RequestersListCreate(generics.ListCreateAPIView):
    queryset = models.Requesters.objects.all()
    serializer_class = RequestersSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = '__all__'

class RequestsListCreate(generics.ListCreateAPIView):
    queryset = models.Requests.objects.all()
    serializer_class = RequestsSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = '__all__'

class StaffListCreate(generics.ListCreateAPIView):
    queryset = models.Staff.objects.all()
    serializer_class = StaffSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = '__all__'

