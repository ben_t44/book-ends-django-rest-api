from rest_framework import serializers
from dbmodels.models import Books, Donations, Donors, Editions, Kids, Persons, Requesters, Requests, Staff

class BooksSerializer(serializers.ModelSerializer):
    class Meta:
        model = Books
        fields = '__all__'
       

class DonationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Donations
        fields = '__all__'

class DonorsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Donors
        fields = '__all__'

class EditionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Editions
        fields = '__all__'

class KidsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kids
        fields = '__all__'

class PersonsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Persons
        feilds = '__all__'

class RequestersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Requesters
        fields = '__all__'

class RequestsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Requests
        fields = '__all__'

class StaffSerializer(serializers.ModelSerializer):
    class Meta:
        model = Staff
        fields = '__all__'

