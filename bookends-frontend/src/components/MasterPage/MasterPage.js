import React from 'react';
import Header from '../Header/Header';
import DocumentTitle from 'react-document-title';
import Home from "../Home/Home"

class MasterPage extends React.Component {

    render() {

        return(
            <DocumentTitle title='My React App'>
                <div className='MasterPage'>
                    { this.props.children ? this.props.children : <Home /> }
                </div>
            </DocumentTitle>
        );
    }
}

export default MasterPage;