from django.contrib import admin

# Register your models here.
from .models import Books, Donations, Donors, Editions, Kids, Persons, Requesters, Requests, Staff 

admin.site.register(Books)
admin.site.register(Donations)
admin.site.register(Donors)
admin.site.register(Editions)
admin.site.register(Kids)
admin.site.register(Persons)
admin.site.register(Requesters)
admin.site.register(Requests)
admin.site.register(Staff)

