from django.urls import path
from .views import BooksListCreate, DonorsListCreate, DonationsListCreate, EditionsListCreate, KidsListCreate, PersonsListCreate, RequestersListCreate, RequestsListCreate, StaffListCreate

urlpatterns = [
    path('api/Books/', BooksListCreate.as_view()),
    path('api/Donations/', DonationsListCreate.as_view()),
    path('api/Donors/', DonorsListCreate.as_view()),
    path('api/Editions/', EditionsListCreate.as_view()),
    path('api/Kids/', KidsListCreate.as_view()),
    path('api/Persons/', PersonsListCreate.as_view()),
    path('api/Requesters/', RequestersListCreate.as_view()),
    path('api/Requests/', RequestsListCreate.as_view()),
    path('api/Staff/', StaffListCreate.as_view()),
]