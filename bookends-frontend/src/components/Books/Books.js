import React from 'react';
import BookView from './BookView/BookView'

var axios = require("axios");
axios.defaults.withCredentials = true;

class Books extends React.Component {

    constructor(props) {
        super(props);
        // Don't call this.setState() here!
        this.state = { data: [], query: ""};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }

    componentDidMount() {
        fetch("http://127.0.0.1:8000/api/Books/")
        .then(res=>res.json())
        .then(
            (results) => {
                this.setState({data: results});
            });

    }

    handleChange(event){
        this.setState({inputfield : event.target.value});
    }

    handleSubmit(event){
        event.preventDefault();
        let component = this;
        if(this.state.inputfield){
            fetch("http://127.0.0.1:8000/api/Books/?title=" + this.state.inputfield)
                .then(res=>res.json())
                .then(
                    (results) => {
                        console.log(results);
                        component.setState({data: results});
                    });

        }
    }

    render() {
        var books;
        if(!this.state.data) {
            books = <div className='text-center'>No Books Came Up</div>
        } else {
            books = <BookView data={this.state.data} />;
        }

        return(
            <div>
                <div className='search text-center'>
                    <form onSubmit={this.handleSubmit} className='form-enter-name'>
                        <h3>Search Book by Title</h3>
                        <label htmlFor='query' className='sr-only'>Enter your user query:</label>
                        <input className='form-control' type='text' id='query' defaultValue={this.state.query} onChange={this.handleChange} />
                        <button className='btn btn-default btn-primary button-size' onClick={this.handleSubmit}>Search</button>
                    </form>

                    {books}
                </div>

                
            </div>
        );
    }
}

export default Books;