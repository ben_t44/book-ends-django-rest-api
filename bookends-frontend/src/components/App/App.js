import React from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom'
import MasterPage from '../MasterPage/MasterPage';
import Books from '../Books/Books';
import BookForm from '../BookForm/BookForm';
import Header from "../Header/Header";
import Footer from '../Footer/Footer';

function App() {
  return (
    <div>
      <Header />
      <Router>
        <div>
          <Route exact path="/" component={MasterPage} />
          <Route path="/books" component={Books} />
          <Route path="/bookform" component={BookForm} />
        </div>
      </Router>
      <Footer />
    </div>
  );
}

export default App;
